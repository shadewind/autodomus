require "autodomus/common/types"
require "json"

module Autodomus; module Types

    shared_examples "a type" do
        it "should survive being converted to JSON and back" do
            json = JSON.generate([subject.describe])
            Types.make_type(JSON.parse(json).first).describe.should eq(subject.describe)
        end
    end

    shared_examples "a singleton type" do
        let(:instance) { described_class.instance }
        let(:class_symbol) { described_class.name.split("::").last.intern }

        it_behaves_like "a type"

        describe ".instance" do
            it "always returns the same object" do
                100.times do
                    described_class.instance.should be(instance)
                end
            end
        end

        describe ".new" do
            it "is private" do
                expect { described_class.new }.to raise_error(NoMethodError)
            end
        end

        describe '#describe' do
            it "returns a symbol which equals the class name" do
                instance.describe.should be(class_symbol)
            end
        end

        describe '#==' do
            it "returns true if the argument is the same instance" do
                (instance == instance).should be_true
            end
        end
    end

    describe Types::Object do
        subject { Types::Object.instance }
        let(:examples) { [ "foobar", 1, :foobar, nil, Class, [], {} ] }

        it_behaves_like "a singleton type"

        describe '#convert' do
            it "preserves identity" do
                examples.each do |item|
                    subject.convert(item).should be(item)
                end
            end
        end

        describe '#is_type_of?' do
            it "returns true for all objects" do
                examples.each do |item|
                    subject.is_type_of?(item).should be_true
                end
            end
        end
    end

    describe Types::Integer do
        subject { Types::Integer.instance }

        it_behaves_like "a singleton type"

        describe '#convert' do
            it "converts valid strings" do
                (0...100).each do |n|
                    subject.convert(n.to_s).should eq(n)
                end
            end

            it "does not convert invalid strings" do
                [ "foobar", "1.2", "1.2.3", ".3", "" ].each do |value|
                    expect { subject.convert(value) }.to raise_error(ConversionError)
                end
            end

            it "does not convert floats" do
                expect { subject.convert(2.0) }.to raise_error(ConversionError)
            end
        end

        describe '#is_type_of?' do
            it "returns true for integers" do
                subject.is_type_of?(10).should be_true
            end

            it "returns false for non-integer objects" do
                [ "foobar", false, 0.1, Class ].each do |e|
                    subject.is_type_of?(e).should be_false
                end
            end
        end
    end

    describe Types::Float do
        subject { Types::Float.instance }

        it_behaves_like "a singleton type"

        describe '#convert' do
            it "converts valid strings" do
                [ 0.1, 0.2, 100.0, 3.2, 0.34 ].each do |value|
                    subject.convert(value.to_s).should be_within(0.01).of(value)
                end
            end

            it "does not convert invalid strings" do
                [ "foobar", "1.2.3", "3..4" ].each do |value|
                    expect { subject.convert(value) }.to raise_error(ConversionError)
                end
            end

            it "converts integers" do
                (1...100).each do |value|
                    subject.convert(value).should be_within(0.01).of(value)
                end
            end
        end

        describe '#is_type_of?' do
            it "returns true for floats" do
                subject.is_type_of?(0.1).should be_true
            end

            it "returns false for non-float types" do
                [ 1, "foobar", false, true, nil, Class ].each do |e|
                    subject.is_type_of?(e).should be_false
                end
            end
        end
    end

    describe Types::Boolean do
        subject { Types::Boolean.instance }

        it_behaves_like "a singleton type"

        describe '#convert' do
            it "converts strings" do
                [ "true", "1" ].each do |value|
                    subject.convert(value).should be(true)
                end

                [ "false", "0" ].each do |value|
                    subject.convert(value).should be(false)
                end
            end

            it "converts integers" do
                subject.convert(1).should be(true)
                subject.convert(0).should be(false)
            end

            it "converts nil to false" do
                subject.convert(nil).should be(false)
            end

            it "accepts true and false" do
                subject.convert(true).should be(true)
                subject.convert(false).should be(false)
            end

            it "does not convert invalid values" do
                [ "foobar", "True", 23, "ture", 0.1, 0.0, 1.0, "0 ", Class ].each do |value|
                    expect { subject.convert(value) }.to raise_error(ConversionError)
                end
            end
        end

        describe '#is_type_of' do
            it "returns true for true, false and nil" do
                [ true, false, nil ].each do |value|
                    subject.is_type_of?(value).should be_true
                end
            end

            it "returns false for non-boolean types" do
                [ "true", "false", 0, 1, Class ].each do |value|
                    subject.is_type_of?(value).should be_false
                end
            end
        end
    end

    describe Types::Symbol do
        subject { Types::Symbol.instance }

        it_behaves_like "a singleton type"

        describe '#convert' do
            it "converts strings" do
                [ "foo", "bar", "baz" ].each do |value|
                    subject.convert(value).should be(value.intern)
                end
            end

            it "preserves symbols" do
                [ :foo, :bar, :baz, :FOO ].each do |value|
                    subject.convert(value).should be(value)
                end
            end

            it "should not convert non-strings" do |value|
                [ 1, 1.0, true, false, nil, Class ].each do |value|
                    expect { subject.convert(value) }.to raise_error(ConversionError)
                end
            end
        end

        describe '#is_type_of?' do
            it "returns true for symbols" do
                subject.is_type_of?(:foobar).should be_true
            end

            it "returns false for non-symbols" do
                [ "foobar", true, false, 1, Class ].each do |value|
                    subject.is_type_of?(value).should be_false
                end
            end
        end
    end

    describe Types::Literal do
        it_behaves_like "a type" do
            subject { described_class.new(:foobar) }
        end

        describe '#convert' do
            it "converts on string equality to value" do
                described_class.new("foobar").convert(:foobar).should eq("foobar")
                described_class.new(1).convert("1").should eq(1)
                described_class.new(true).convert("true").should be_true
            end

            it "does not convert non-matching values" do
                literal = described_class.new("foobar")
                [ "foo", "bar", :foo, :bar].each do |value|
                    expect { literal.convert(value) }.to raise_error(ConversionError)
                end
            end
        end

        describe '#is_type_of?' do
            it "returns true for equal objects" do
                described_class.new(:foobar).is_type_of?(:foobar).should be_true
            end

            it "returns false for non-equal object" do
                described_class.new(:foobar).is_type_of?("foobar").should be_false
            end
        end

        describe '#describe' do
            it "returns the literal value for non-symbol values" do
                [ 1, 2.0, "foobar", true, false, Object ].each do |value|
                    described_class.new(value).describe.should eq(value)
                end
            end

            it "returns a string starting with a colon for symbol values" do
                described_class.new(:foobar).describe.should eq(":foobar")
            end
        end

        describe '#==' do
            it "returns true for equal literals" do
                (described_class.new("foobar") == described_class.new("foobar")).should be_true
            end

            it "returns false for non-equal literals" do
                (described_class.new("foobar") == described_class.new("foobaz")).should be_false
            end
        end
    end

    describe Types::Array do
        it_behaves_like "a type" do
            subject { described_class.new(:String) }
        end

        describe '#convert' do
            it "doesn't accept non-arrays" do
                array_type = described_class.new(:Object)
                [ true, false, "foobar", 1, 0.0, Object ].each do |value|
                    expect { array_type.convert(value) }.to raise_error(ConversionError)
                end
            end

            it "converts all elements to the type" do
                array_type = described_class.new(:String)
                values = [ 1, 2, false, "foobar", :foobar ]
                expected = [ "1", "2", "false", "foobar", "foobar" ]
                array_type.convert(values).should eq(expected)
            end

            it "requires all elements to be convertable" do
                array_type = described_class.new(:Integer)
                values = [ "1", "2", "foo" ]
                expect { array_type.convert(values) }.to raise_error(ConversionError)
            end
        end

        describe '#is_type_of?' do
            let(:array_type) { array_type = described_class.new(:Symbol) }

            it "returns true if all elements match" do
                array_type.is_type_of?([ :foo, :bar, :baz ]).should be_true
            end

            it "returns false if any element does not match" do
                array_type.is_type_of?([ :foo, :bar, "baz" ]).should be_false
            end
        end

        describe '#describe' do
            it "should return a nested array" do
                result = described_class.new(:String).describe
                result.should be_a_kind_of(::Array)
                result.first.should be_a_kind_of(::Array)
            end

            context "when type is Any" do
                it "should return an array nested only once" do
                    result = described_class.new([:String]).describe
                    result.should be_a_kind_of(::Array)
                    result.first.should be_a_kind_of(::Array)
                    result.first.first.should be(:String)
                end
            end
        end

        describe '#==' do
            it "should return true for equal types" do
                (described_class.new(:String) == described_class.new(:String)).should be_true
            end

            it "should return false for non-equal types" do
                (described_class.new(:String) == described_class.new(:Integer)).should be_false
            end
        end
    end

    describe Types::Hash do
        describe '#convert' do
            it "converts both key and value" do
                hash_type = described_class.new({ :Integer => :String })
                input = { "10" => 2, "100" => false }
                expected = { 10 => "2", 100 => "false" }
                hash_type.convert(input).should eq(expected)
            end

            it "prefers perfect matches" do
                hash_type = described_class.new({ :String => :String, :foobar => :Integer })
                input = { :foobar => 10, :foobaz => "10" }
                expected = { :foobar => 10, "foobaz" => "10" }
                hash_type.convert(input).should eq(expected)
            end

            it "tries conversions in order of declaration if no perfect match is found" do
                hash_type = described_class.new({ :my_literal => :Integer, :Symbol => :Symbol })
                input = { "my_literal" => "10", "foobar" => "10" }
                expected = { :my_literal => 10, :foobar => :"10" }
                hash_type.convert(input).should eq(expected)
            end

            it "ignores pairs for which there is no match" do
                hash_type = described_class.new({ :Integer => :Integer, :Float => :Float })
                hash_type.convert({ "1" => "2", foo: "bar" }).should eq({ 1 => 2 })
            end

            it "raises error on non-hash objects" do
                expect { described_class.new({ 1 => 2 }).convert("foo") }.to raise_error(ConversionError)
            end
        end

        describe '#is_type_of?' do
            it "returns true when all pairs match" do
                hash_type = described_class.new({ :Integer => :Integer, :Float => :Float })
                input = { 1.0 => 1.0, 1 => 1, 2.0 => 2.0 }
                hash_type.is_type_of?(input).should be_true
            end

            it "returns false when any pair does not match" do
                hash_type = described_class.new({ :Integer => :Integer, :Float => :Float })
                input = { 1.0 => "1.0", 1 => 1, 2.0 => 2.0 }
                hash_type.is_type_of?(input).should be_false
            end
        end

        describe '#==' do
            it "returns true for identical hash types" do
                hash = { :Integer => :Symbol, :Float => :Symbol }
                (described_class.new(hash) == described_class.new(hash)).should be_true
            end

            it "returns false for non-identical hash types" do
                hash1 = { :Integer => :Symbol }
                hash2 = { :Integer => :Symbol, :Float => :Symbol }
                (described_class.new(hash1) == described_class.new(hash2)).should be_false
            end
        end
    end

    describe Types::Any do
        describe '#convert' do
            it "prefers a perfect match" do
                any_type = described_class.new([ :Integer, :String ])
                any_type.convert("1").should eq("1")
            end

            it "tries conversions in order if a perfect match is not found" do
                any_type = described_class.new([ :Float, :Integer ])
                any_type.convert("1").should be_a_kind_of(::Float)
            end

            it "fails if no suitable conversion is found" do
                any_type = described_class.new([ :Integer, :Float ])
                expect { any_type.convert("foo") }.to raise_error(ConversionError)
            end
        end

        describe '#is_type_of?' do
            it "returns true if any of the types is a match" do
                described_class.new([ :Integer, :String ]).is_type_of?(1).should be_true
            end

            it "returns false if none of the types is a match" do
                described_class.new([ :Integer, :String ]).is_type_of?(:foo).should be_false
            end
        end

        describe '#==' do
            it "returns true for identical type lists" do
                types = [ :Integer, :String ]
                (described_class.new(types) == described_class.new(types)).should be_true
            end

            it "returns false for non-identical type lists" do
                types1 = [ :Integer, :String ]
                types2 = [ :String, :Integer ]
                (described_class.new(types1) == described_class.new(types2)).should be_false

                types1 = [ :Integer, :String ]
                types2 = [ :Integer, :String, :Symbol ]
                (described_class.new(types1) == described_class.new(types2)).should be_false
            end
        end
    end

    describe Types do
        describe ".make_type" do
            it "returns the singleton instance for singleton types" do
                ObjectSpace.each_object(Class).select do |obj|
                    # Select all singleton types
                    obj < Types::SingletonType
                end.each do |obj|
                    # Take their class name
                    Types.make_type(obj.name.split("::").last.intern).should be(obj.instance)
                end
            end

            it "returns a Types::Hash for hashes" do
                Types.make_type({}).should be_a_kind_of(Types::Hash)
            end

            it "returns a Types::Any for non-nested arrays" do
                Types.make_type([ :String ]).should be_a_kind_of(Types::Any)
            end

            context "when array is nested" do
                it "returns Types::Any when outer_array.length != 1" do
                    Types.make_type([[ :String ], :Integer ]).should be_a_kind_of(Types::Any)
                end

                it "returns Types::Array when outer_array.length == 1" do
                    Types.make_type([[ :String ]]).should be_a_kind_of(Types::Array)
                end
            end

            it "returns the given object if it's already a type" do
                [ Types::String, Types::Integer, Types::Float ].each do |klass|
                    Types.make_type(klass.instance).should be(klass.instance)
                end
            end

            it "returns Types::Literal if there is no match" do
                [ :foobar, 10, "hey", Object ].each do |obj|
                    Types.make_type(obj).should be_a_kind_of(Types::Literal)
                end
            end
        end
    end

end end