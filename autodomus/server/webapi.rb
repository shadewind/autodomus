require "sinatra"
require "json"
require "multi_json"
require "sinatra/json"

require "autodomus/common/types"

module Autodomus; module Server

    class WebApi < Sinatra::Base
        set :environment, :development
        set :show_exceptions, :after_handler
        set :dump_errors, false unless development?
        
        helpers Sinatra::JSON

        def initialize(context)
            super()
            @context = context
        end

        # Returns summary info about an entity.
        get "/api/entity/?*" do
            json @context.describe(entity_path)
        end

        # Takes actions on entities.
        post "/api/entity/?*" do
            apply_actions(entity_path, request.body.read)
        end

        # Returns summary info about tags.
        get "/api/tags/?" do
            return json @context.tags if params.empty?
            json @context.describe(params.keys)
        end

        # Take actions on tags.
        post "/api/tags/?" do
            apply_actions(params.keys, request.body.read)
        end

        error Autodomus::NotFoundError do
            [404, env["sinatra.error"].message]
        end

        error Autodomus::AutodomusError do
            [400, env["sinatra.error"].message]
        end

        error JSON::JSONError do
            [400, "Invalid JSON format: #{env['sinatra.error'].message}"]
        end

        private

        def entity_path
            params[:splat].first.gsub("/", ".")
        end

        def apply_actions(target, action_data)
            actions = JSON.parse(action_data)
            halt 400, "Root must be an array" unless actions.is_a?(Array)
            
            actions.each do |action_hash|
                halt 400, "Action must be a hash" unless action_hash.is_a?(Hash)
                halt 400, "Action is empty" if action_hash.empty?
                action, data = action_hash.first

                case action
                when "set_properties"
                    apply_set_properties(target, data)
                when "call_procedures"
                    apply_call_procedures(target, data)
                else
                    halt 400, "Unknown action #{action}"
                end
            end

            json @context.describe(target)
        end

        def apply_set_properties(target, data)
            unless data.is_a?(Hash)
                halt 400, "Data for 'set_property' must be a hash"
            end

            data.each do |k,v|
                @context.set_property(target, k.to_s, v)
            end
        end

        def apply_call_procedures(target, data)
            unless data.is_a?(Hash)
                halt 400, "Data for 'call_procedures' must be a hash"
            end

            # Validate first
            data.each do |k,v|
                unless v.is_a?(Array)
                    halt 400, "Value for procedure '#{k}' must be an array"
                end
            end

            data.each do |k,v|
                @context.call_procedure(target, k.to_s, v)
            end
        end
    end

end end