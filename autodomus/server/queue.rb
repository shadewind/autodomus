require "thread"

module Autodomus; module Server

    # Blocking queue implementation.
    class Queue

        def initialize
            @queue = []
            @mutex = Mutex.new
            @cond = ConditionVariable.new
        end

        # Adds an item to the queue.
        def <<(item)
            @mutex.synchronize do
                @queue.unshift(item)
                @cond.signal
            end
        end

        # Waits until items are available or until timeout.
        def wait(timeout = nil)
            @mutex.synchronize do
                @cond.wait(@mutex, timeout) if @queue.empty?
            end

            nil
        end

        def next
            @mutex.synchronize { @queue.pop }
        end

        def empty?
            @mutex.synchronize { @queue.empty? }
        end
    end

end end