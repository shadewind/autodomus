module Autodomus; module Server

    # Basic worker thread class.
    class Worker
        def initialize
            @queue = []
            @mutex = Mutex.new
            @run = true
            @thread = Thread.new(&method(:run))
        end

        # Submits a block to be executed by the worker.
        def submit(&block)
            @mutex.synchronize do
                @queue.unshift(block)
                @thread.wakeup
            end
        end

        # Stops the worker.
        def stop
            @mutex.synchronize do
                @run = false
                @thread.wakeup
            end
        end

        private

        def run
            @mutex.lock
            while @run
                @mutex.sleep while @queue.empty?
                work_item = @queue.pop
                
                @mutex.unlock
                work_item.call
                @mutex.lock
            end
            @mutex.unlock
        end
    end

end end