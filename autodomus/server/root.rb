require "monitor"

require "autodomus/server/entity"
require "autodomus/server/aggregate_entity"
require "autodomus/server/triggerable"
require "autodomus/server/queue"
require "autodomus/common/accessor"

module Autodomus; module Server

    # Root of the entity tree.
    class Root < Entity
        include MonitorMixin
        
        def initialize
            super(name: "", description: nil)
            @subscriptions = {}
            @properties = {}
            @procedures = []

            @event_queue = Server::Queue.new
        end

        # Returns an +AggregateEntity+ for the given set of tags.
        def tag(*args)
            entities = search.select do |entity|
                args.all? { |t|entity.tags.include?(t) }
            end
            AggregateEntity.new(entities)
        end

        # Sets a block to be called when an event occurs.
        def on(type = Server::Event, &block)
            unless @subscriptions.has_key?(type)
                @subscriptions[type] = []
            end

            @subscriptions[type] << block
        end

        def post_event(event)
            @event_queue << event
        end

        def start
            super
            @thread = Thread.new(&method(:run))
        end

        private

        def run
            loop do
                future = next_tick
                wait_time =  future.nil? ? nil : future - Time.now
                @event_queue.wait(wait_time)
                tick
                process_event(@event_queue.next) until @event_queue.empty?
            end
        end

        def process_event(event)
            @subscriptions.each_pair do |type, subs|
                subs.each { |s| s.call(event) } if event.is_a?(type)
            end

            on_event(event)
        end
    end

end end