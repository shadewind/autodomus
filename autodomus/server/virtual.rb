require "autodomus/server/entity"

module Autodomus; module Server

    # This entity contains no properties or procedures of its own but lets the user
    # create them through scripts in the config.
    class Virtual < Entity

        def initialize(properties: {}, procedures: {}, **args)
            super(args)

            setup_properties(properties)
            setup_procedures(procedures)
        end

        def setup_properties(properties)
            properties.each_pair do |name, hash|
                unless hash.is_a?(Hash)
                    hash = { value: hash.to_s }
                end

                property = VirtualProperty.new(
                    parent: self,
                    name: name,
                    type: hash[:type] || :Object,
                    script: hash[:value])
                property.configure_triggerable(hash, self)

                add_triggerable(property)
                add_property(property)
            end
        end

        def setup_procedures(procedures)
            procedures.each_pair do |name, hash|
                unless hash.is_a?(Hash)
                    hash = { do: hash.to_s }
                end

                unless hash.has_key?(:do)
                    raise ConfigrationError, "No script given for procedure '#{name}'"
                end

                procedure = VirtualProcedure.new(
                    parent: self,
                    name: name,
                    script: hash[:do])
                procedure.configure_triggerable(hash, self)

                add_triggerable(procedure)
                add_procedure(procedure)
            end
        end

        class VirtualProperty < Property
            include GenericTriggerable

            def initialize(script: nil, **args)
                super(args)
                @script = script
            end

            def start
                super
                triggers_from_script(@script, parent) unless @script.nil?
                on_trigger
            end

            def writable?
                @script.nil?
            end

            def readable?
                true
            end

            def get
                super

                @value
            end

            def set(value)
                super

                do_set(value)
            end

            def on_trigger
                do_set(parent.evaluate(@script)) unless @script.nil?
            end

            protected

            def do_set(value)
                property_name = name
                setter = Proc.new { @value = value }
                parent.instance_eval { modify(property_name, &setter) }
            end
        end

        class VirtualProcedure < Procedure
            include GenericTriggerable

            def initialize(script: nil, **args)
                args[:types] = []
                super(args)
                @script = script
            end

            def on_trigger
                invoke
            end

            def invoke
                parent.evaluate(@script)
            end
        end
    end

end end