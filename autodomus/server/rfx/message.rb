module Autodomus; module Server; module Rfx

    # Read defines from header file
    rfx_header = IO.read("#{File.dirname(__FILE__)}/RFXtrx.h")
    rfx_header.each_line.select { |l| l.start_with?("#define") }.each do |l|
        if l =~ /#define\s+(\w+)\s+(0x[0-9]+)/
            name = $1[0].upcase + $1[1..-1]
            Rfx.const_set(name, $2.hex)
        end
    end

    # Define fields which are common to all packets
    class Message < BinData::Record
        # Length of the packet
        uint8 :packetlength, :value => lambda { num_bytes - 1 }
        # The type of the packet
        uint8 :packettype
        # The subtype of the packet
        uint8 :subtype
        # The sequence number of the packet
        uint8 :seqnbr
    end

    # Base class for payload classes
    class Payload < BinData::Record
        @subclasses = []

        # Assigns the values of the specified hash to the fields of this
        # message.
        def assign_hash(field_values)
            field_values.each_pair do |k, v|
                if field_names.include?(k.to_s)
                    self[k] = v
                end
            end
        end

        def inspect
            self.class.name + " {\n" + field_names.map do |name|
                "    #{name}: #{self[name].snapshot}\n"
            end.join + "}\n"
        end

        class << self
            # Returns a list of all the subclasses to this class.
            attr_reader :subclasses

            def inherited(klass)
                @subclasses << klass
            end
        end
    end

    # Some payloads have differing type and struct names so map them manually
    TYPE_MAPPINGS = {
        "ICMND" => PTypeInterfaceControl,
        "IRESPONSE" => PTypeInterfaceMessage,
        "RXRESPONSE" => PTypeRecXmitMessage
    }

    # Replacements for field names (some are reserved et.c.)
    FIELD_MAPPINGS = { 
        "min" => "minimum",
        "count" => "num"
    }
  
    # Store what the common fields so we don't readd them
    common_fields = Message.new.field_names
    # Dynamically define BinData records
    rfx_header.scan(/struct \s+ { ([^}]*) } \s* (\w+) \s*;/x) do |data, struct_name|
        klass = Class.new(Payload)
        Rfx.const_set(struct_name, klass)
        BinData::RegisteredClasses.register(struct_name.downcase, klass)

        # Use manual mapping for type constant if it exists
        packettype = TYPE_MAPPINGS[struct_name]
        # Otherwise search constants
        if packettype.nil?
            # If name is FOO_BAR1, try FOO_BAR1, FOOBAR1 and FOO_BAR
            class_name = klass.name.split("::").last
            tries = [
                class_name,
                class_name.gsub("_", ""),
                class_name[/[^\d]+/]
            ]
            constant_name = Rfx.constants.find do |c|
                tries.any? { |try| c.to_s.casecmp("PType#{try}") == 0 }
            end
            packettype = Rfx.const_get(constant_name)
        end
        klass.define_singleton_method(:packettype) { packettype }

        # Bit fields need to be reversed in groups of 8 so find them first...
        field_size = 0
        bitfields = []
        fields = []
        data.scan(/^\s+BYTE \s+ (\w+) (?:\s*:\s* (\d+) )?;/x) do |name, length|
            length = length.nil? ? 8 : length.to_i
            field_size += length
            bitfields << [ name, length ]
            if field_size >= 8
                fields.concat(bitfields.reverse)
                field_size = 0
                bitfields = []
            end
        end

        # ... then add the fields to the class
        fields.each do |name, length|
            next if common_fields.include?(name)
            type = (length == 8) ? "uint8" : "bit#{length}"
            mapping = FIELD_MAPPINGS[name]
            name = mapping unless mapping.nil?
            klass.send(type, name.downcase)
        end        
    end

    # Define the payload field
    class Message
        choice :payload, :selection => :packettype do
            Payload.subclasses.each do |klass|
                send(klass.name.downcase, klass.packettype)
            end
            payload :default
        end

        # Creates a new Message. If passed class to the parameter +packettype+,
        # the packet type is set to +type.packettype+. If more values are passed,
        # the corresponding fields are set to its values
        def self.create(type: nil, device: nil, **field_values)
            msg = Message.new
            msg.packettype = type.packettype unless type.nil?

            unless device.nil?
                msg.packettype = device.class.packettype
                msg.assign_hash(device.id)
            end

            msg.assign_hash(field_values)
            msg
        end

        # Assigns the values of the specified hash to the fields of this
        # message.
        def assign_hash(hash)
            field_values = hash.dup
            field_values.each_pair do |k, v|
                if field_names.include?(k.to_s)
                    self[k] = v
                    field_values.delete(k)
                end
            end

            # Assign the rest to the payload
            payload.assign_hash(field_values)
        end

        # Returns a hash of all the fields of the payload named +packettype+, +subtype+, +id*+ or +*code+
        # This can be used to identify a particular device.
        def id
            ident = { packettype: self[:packettype].snapshot, subtype: self[:subtype].snapshot }
            payload.field_names.select { |f| f.to_s.start_with?("id") || f.to_s.end_with?("code") }.each do |f|
                ident[f.intern] = payload[f].snapshot
            end

            ident
        end

        # Returns the name of the packet type.
        def type_name
            payload_class = Payload.subclasses.find { |c| c.packettype == packettype }
            payload_class = Payload if payload_class.nil?
            payload_class.name.split("::").last
        end

        def inspect
            str = type_name + " {\n"
            field_names.select { |n| n != "payload" }.each do |name|
                str << "    #{name}: #{self[name].snapshot}\n"
            end
            payload.field_names.each do |name|
                str << "    #{name}: #{payload[name].snapshot}\n"
            end
            str << "}\n"
            str
        end

        # Returns a hex string representation of the message
        def hex_string
            to_binary_s.each_byte.map { |b| sprintf("%02X", b) }.join(" ")
        end
    end

end end end