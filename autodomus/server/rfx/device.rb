require "monitor"

module Autodomus; module Server; module Rfx

    # Base class for Rfx devices.
    class Device < Server::Entity

        # The ID of the device
        attr_reader :id

        # Initializes this +Device+ with the specified id.
        def initialize(id: nil, **args)
            super(args)
            raise Server::ConfigurationError, "ID must be provided for RFX devices" if id.nil?
            @id = id
            @id[:packettype] = self.class.packettype unless self.class.packettype.nil?
            @rssi = 0
            @monitor = Monitor.new
        end

        # Returns the packet type that this device class handles.
        def self.packettype
            nil
        end

        # Returns true if the specified message is intended for this device.
        def handles_message(msg)
            msg.id == @id
        end

        # Handles the specified message.
        def handle_message(msg)
            synchronize do
                modify(:rssi) { @rssi = msg.payload.rssi }
            end
        end

        def parent=(entity)
            unless entity.is_a?(RfxTrx)
                raise ConfigurationError, "Parent of Rfx::Device must be RfxTrx"
            end
            super
        end

        def rssi
            synchronize { @rssi }
        end

        protected

        def synchronize(&block)
            @monitor.synchronize &block
        end

        add_property :id, type: { :Symbol => :Integer }
        add_property :rssi, type: :Integer
    end

end end end