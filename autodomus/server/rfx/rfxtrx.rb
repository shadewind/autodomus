require "serialport"
require "bindata"
require "monitor"
require "io/console"
require "json"

require "autodomus/server/entity"
require "autodomus/server/rfx/message"
require "autodomus/server/rfx/device"

module Autodomus; module Server; module Rfx

    class RfxTrx < Server::Entity

        def initialize(device: nil, enabled_protocols: [ "AC" ], **args)
            super(args)

            if device.nil?
                raise ConfigurationError, "Device must be specified for RFXtrx"
            end
            @device = device
            @enabled_protocols = enabled_protocols

            @response_callbacks = []
            @devices = []
            @monitor = Monitor.new
        end

        # Sends a message. If a block is given, the block is called when a
        # response is received.
        def call_async(msg, &block)
            @monitor.synchronize do
                @response_callbacks.unshift(block.nil? ? Proc.new {} : block)
                send_msg(msg)
            end
        end

        # Sends a message and waits for the reply.
        def call(msg)
            response_mutex = Mutex.new
            response_mutex.synchronize do
                response = nil
                this_thread = Thread.current
                
                call_async(msg) do |r|
                    response_mutex.synchronize do
                        response = r
                        this_thread.wakeup
                    end
                end

                response_mutex.sleep while response.nil?
                response
            end
        end

        # Returns a list of name for enabled protocols.
        def enabled_protocols
            @enabled_protocols
        end

        def add_child(entity)
            unless entity.is_a?(Rfx::Device)
                raise ConfigurationError, "Children of RfxTrx must be Rfx::Device"
            end
            super
        end

        def start
            reset
            @thread = Thread.new { read_loop }
            init

            super
        end

        private

        def reset
            if @port.nil?
                @port = SerialPort.new(@device, 38400, 8, 1, SerialPort::NONE)
                @port.binmode
                @port.read_timeout = 0
            end

            send_msg(Message.create(type: ICMND))
            @port.iflush
            sleep 1
        end

        def init
            msg = Message.create(
                type: ICMND,
                cmnd: CmdSETMODE,
                msg1: TrxType43392)

            Rfx.constants.select { |c| @enabled_protocols.include?(c[/^Msg[3-5]_(.*)$/, 1]) }.each do |c|
                field = c.to_s[0,4].downcase
                msg.payload[field] |= Rfx.const_get(c)
            end

            @enabled_protocols = protos_from_msg(call(msg))
            info "Enabled protocols: " + @enabled_protocols.join(", ")
        end

        def protos_from_msg(msg)
            en = msg.payload.field_names.select { |f| f =~ /^(rfu.*|.*enabled)$/ }.map { |f| msg.payload[f] }
            bytes = en.each_slice(8).map do |slice|
                slice.each_with_index.reduce(0) { |sum, x| sum | (x[0] << (7 - x[1])) }
            end

            Rfx.constants.select do |c|
                (c =~ /^Msg([3-5])_/) && ((bytes[$1.to_i - 3] & Rfx.const_get(c)) != 0)
            end.map do |c|
                c[5..-1]
            end
        end

        def send_msg(msg)
            msg.write(@port)
            @port.flush
        end

        def recv_msg
            len_byte = @port.getc
            length = len_byte.getbyte(0)
            data = @port.read(length)
            raise EOFError, "Read EOF from serial port" if data.length < length
            Message.read(len_byte + data)
        end

        def read_loop
            until @port.closed?
                msg = recv_msg

                case msg.packettype
                when PTypeRecXmitMessage, PTypeInterfaceMessage then on_response(msg)
                else on_receive(msg)
                end
            end
        end

        def on_receive(msg)
            devices = children.select { |d| d.handles_message(msg) }
            unless devices.empty?
                devices.each { |d| d.handle_message(msg) }
            else
                info("Unknown #{msg.type_name} device: #{JSON.generate(msg.id)}")
            end
        end

        def on_response(msg)
            @monitor.synchronize do 
                callback = @response_callbacks.pop
                callback.call(msg) unless callback.nil?
            end
        end

        add_property :enabled_protocols, type: [[ :String ]]
    end

end end end