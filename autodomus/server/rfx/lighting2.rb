require "autodomus/server/rfx/device"
require "autodomus/server/rfx/message"

module Autodomus; module Server; module Rfx

    # Corresponds to a device 
    class Lighting2Device < Rfx::Device

        def initialize(**args)
            super(args)
            @on = false
            @level = 0
        end

        def level
            synchronize { @level }
        end

        def level=(value)
            return if value.nil?
            
            synchronize do
                parent.call_async Message.create(
                    device: self,
                    cmnd: Light2_sSetLevel,
                    level: value)
                modify(:level) { @level = value }
            end
        end

        def on
            synchronize { @on }
        end

        def on=(value)
            synchronize do
                parent.call_async Message.create(
                    device: self,
                    cmnd: value ? Light2_sOn : Light2_sOff)
                modify(:on) { @on = value }
            end
        end

        # Toggles the on/off state
        def toggle
            synchronize { self.on = !self.on }
        end

        def self.packettype
            LIGHTING2.packettype
        end

        def handles_message(msg)
            msg_group_id = msg.id.select { |k,v| k.to_s.start_with?("id") }
            self_group_id = @id.select { |k,v| k.to_s.start_with?("id") }
            msg_group_id == self_group_id
        end

        def handle_message(msg)
            super
            
            synchronize do
                modify(:on, :level) do
                    case msg.payload.cmnd
                    when Light2_sOn
                        @on = true if msg.payload.unitcode == @id[:unitcode]
                    when Light2_sOff
                        @on = false if msg.payload.unitcode == @id[:unitcode]
                    when Light2_sSetLevel
                        @level = msg.level if msg.payload.unitcode == @id[:unitcode]
                    when Light2_sGroupOn
                        @on = true
                    when Light2_sGroupOff
                        @on = false
                    end
                end
            end
        end

        # The dimmer level, 0 - 15
        add_property :level, type: [ :Integer ]
        # The on/off state
        add_property :on, type: [ :Boolean ]

        add_procedure :toggle
    end

end end end