require "autodomus/server/rfx/device"
require "autodomus/server/rfx/message"

module Autodomus; module Server; module Rfx

    # Corresponds to a device 
    class TempDevice < Rfx::Device

        def initialize(**args)
            super(args)
            @temperature = 0.0
            @battery_level = 0.0
        end

        def temperature
            synchronize { @temperature }
        end

        def battery_level
            synchronize { @battery_level }
        end

        def handle_message(msg)
            super

            synchronize do
                modify(:temperature, :battery_level) do
                    temp = (msg.payload.temperatureh << 8) | msg.payload.temperaturel
                    sign = (msg.payload.tempsign == 1) ? -1 : 1
                    @temperature = (temp * sign) / 10.0

                    @battery_level = msg.payload.battery_level / 9.0
                end
            end
        end

        add_property :temperature, type: :Float
        add_property :battery_level, type: :Float
    end

end end end