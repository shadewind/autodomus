require "monitor"

require "autodomus/server/entity"
require "autodomus/common/jsonrpc"

module Autodomus; module Server

    # Connects via JSON RPC and provides info about an XBMC instance.
    class Xbmc < Entity
        def initialize(hostname: "localhost", port: 9090, **args)
            super(args)

            @monitor = Monitor.new
            @rpc = JsonRpc::TcpClient.new(hostname, port)
            @rpc.on_connect(&method(:on_connect))
            @rpc.on_notification(&method(:on_notification))
            @rpc.on_disconnect(&method(:on_disconnect))

            @running = false
            @playing = false
        end

        def start
            @rpc.auto_connect(10)
            
            super
        end

        def playing
            @monitor.synchronize { @playing }
        end

        def running
            @rpc.connected?
        end

        private

        def on_connect
            modify(:running) { @running = true }
            info "Connected to #{@rpc.host}:#{@rpc.port}"

            @rpc.call_method("Player.GetActivePlayers") do |result|
                unless result.is_a?(Array)
                    warn "Player.GetActivePlayers response is not array"
                    next
                end
                
                if result.empty?
                    @monitor.synchronize { modify(:playing) { @playing = false } }
                    next
                end

                player = result.first
                
                unless player.has_key?("playerid")
                    warn "No player ID in response to Player.GetActivePlayers"
                    next
                end

                params = { 
                    "playerid" => player["playerid"],
                    "properties" => [ "speed" ]
                }
                @rpc.call_method("Player.GetProperties", params) do |result|
                    unless result.is_a?(Hash)
                        warn "Player.GetProperties is not a hash"
                        next
                    end

                    @monitor.synchronize do
                        modify(:playing) { @playing = (result["speed"] != 0) }
                    end
                end
            end
        end

        def on_notification(method, params)
            @monitor.synchronize do
                case method
                when "Player.OnPlay"
                    modify(:playing) { @playing = true }
                when "Player.OnPause", "Player.OnStop"
                    modify(:playing) { @playing = false }
                end
            end
        end

        def on_disconnect
            info "Disconnected from #{@rpc.host}:#{@rpc.port}"
            @monitor.synchronize do
                modify(:playing, :running) do
                    @playing = false
                    @running = false
                end
            end
        end

        add_property :playing, type: :Boolean
        add_property :running, type: :Boolean
    end

end end