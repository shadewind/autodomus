require "logger"

require "autodomus/common/entity_member"
require "autodomus/common/entity_nav"
require "autodomus/server/event"
require "autodomus/common/logging_helpers"
require "autodomus/common/entity_base"
require "autodomus/server/triggerable"

module Autodomus; module Server

    # Thrown to indicate configuration error.
    class ConfigurationError < StandardError; end

    class UnboundProperty
        attr_reader :name, :type

        def initialize(name, read: nil, write: nil, type: :Object)
            raise ArgumentError, "Neither writable or readable" if (read.nil? && write.nil?)
            raise ArgumentError, "Invalid reader" unless [ NilClass, Proc, Symbol ].include?(read.class)
            raise ArgumentError, "Invalid writer" unless [ NilClass, Proc, Symbol ].include?(write.class)

            @name = name.intern
            @type = Types.make_type(type)
            @read = read
            @write = write
        end

        # Checks whether this property is readable for the given target.
        def readable?(target)
            case @read
            when nil then false
            when Proc then true 
            when Symbol then target.respond_to?(@read)
            end
        end

        # Checks whether this procedure is writable for the given target.
        def writable?(target)
            case @write
            when nil then false
            when Proc then true
            when Symbol then target.respond_to?(@write)
            end
        end

        # Gets the property described by this object from the given target.
        def get(target)
            case @read
            when Proc then target.instance_eval(&@read)
            when Symbol then target.send(@read)
            end
        end

        # Sets the property described by this object on the target to the given value.
        # An attempt is made to convert the value to the type of this property and a
        # +ConversionError+ is raised if the value could not be converted.
        def set(target, value)
            converted_value = @type.convert(value)
            case @write
            when Proc then target.instance_exec(converted_value, &@write)
            when Symbol then target.send(@write, converted_value)
            end
        end
    end

    class UnboundProcedure
        attr_reader :name

        def initialize(name, impl: nil, types: [])
            raise ArgumentError, "No implementation" if impl.nil?
            @name = name.intern
            @types = types.map { |t| Types.make_type(t) }

            @impl = case impl
            when Symbol, String then Proc.new { |*args| send(impl, *args) }
            when Proc then impl
            else raise ArgumentError, "Impl is neither a String/Symbol nor a Proc"
            end
        end

        # The types of the arguments.
        def types
            @types.to_enum
        end

        # Calls this procedure on the given target.
        def invoke(target, *args)
            unless args.length == @types.length
                raise AutodomusError, "Wrong number of arguments to '#{@name}', #{args.length} for #{@types.length}"
            end
            
            converted_args = args.zip(@types).map { |arg, type| type.convert(arg) }
            target.instance_exec(*args, &@impl)
        end
    end

    # Adapter which binds a property to an instance.
    class BoundProperty < Property
        attr_reader :parent

        def initialize(parent, unbound)
            @parent = parent
            @unbound = unbound
        end

        def name
            @unbound.name
        end

        def type
            @unbound.type
        end

        def writable?
            @unbound.writable?(parent)
        end

        def readable?
            @unbound.readable?(parent)
        end

        def set(value)
            super
            @unbound.set(parent, value)
        end

        def get
            super
            @unbound.get(parent)
        end
    end

    # Adapter which binds a procedure to an instance.
    class BoundProcedure < Procedure
        attr_reader :parent

        def initialize(parent, unbound)
            @parent = parent
            @unbound = unbound
        end

        def name
            @unbound.name
        end

        def types
            @unbound.types
        end

        def invoke(*args)
            @unbound.invoke(@parent, *args)
        end
    end

    # Binds a property to the value of a Ruby script.
    class PropertyBinding
        include GenericTriggerable

        def initialize(property, script)
            @property = property
            @script = script
        end

        def start
            super
            triggers_from_script(@script, @property.parent)
        end

        def on_trigger
            # TODO handle user script errors
            @property.set(@property.parent.evaluate(@script))
        end
    end

    # Implements base functionality for Autodomus entities.
    class Entity < EntityBase
        include EntityNavigation
        include LoggingHelpers

        # The entity name. Is used as an identifier
        attr_reader :name
        # A short description of the entity (i.e. Kitchen lamp)
        attr_reader :description
        # The parent entity
        attr_reader :parent

        def initialize(name: "", description: "", tags: [], bind: {}, **args)
            super()
            @properties = self.class.properties.map { |p| BoundProperty.new(self, p) }
            @procedures = self.class.procedures.map { |p| BoundProcedure.new(self, p) }
            @children = []
            @name = name
            @description = description
            @tags = tags.map(&:to_s)
            @state_stack = []
            @triggerables = []

            setup_bindings(bind)
        end

        def setup_bindings(bindings)
            raise ConfigurationError, "Bindings must be a hash" unless bindings.is_a?(Hash)

            bindings.each_pair do |name, script|
                property = property(name)
                if property.nil?
                    raise ConfigurationError, "Cannot bind non-existant property '#{name.to_s}'"
                end

                add_triggerable(PropertyBinding.new(property, script))
            end
        end

        # Returns a array of the properties for this object.
        def properties
            @properties.to_enum
        end

        # Returns a array of the unbound properties for this class.
        def self.properties
            superclass_props = (self == Entity) ? [] : superclass.properties
            superclass_props + (@properties || [])
        end

        # Returns an array of the unbound procedures for this object.
        def procedures
            @procedures.to_enum
        end

        # Returns an array of the unbound procedures for this object.
        def self.procedures
            superclass_procs = (self == Entity) ? [] : superclass.procedures
            superclass_procs + (@procedures || [])
        end

        # Returns all of the children of this entity.
        def children
            @children.to_enum
        end

        # Adds a child entity.
        def add_child(entity)
            unless entity.parent.nil?
                raise ArgumentError, "Entity already has a parent: #{entity.path}"
            end

            @children << entity
            entity.parent = self
        end

        # The tags of this entity
        def tags
            @tags.dup
        end

        # Pushes the current state onto the state stack.
        def push_state
            props = properties.select { |p| p.readable? && p.writable? }
            @state_stack.push Hash[props.map(&:get)]
        end

        # Pops the current state from the state stack.
        def pop_state
            return if @state_stack.empty?

            @state_stack.pop.each_pair do |k,v|
                k.set(v)
            end
        end

        # Starts entity processing. After this method is called, all outside visible state
        # except property values and remote procedure return values must not change. Children
        # must not be added.
        def start
            triggerables.each(&:start)
        end

        # Called to notify this entity about an event.
        def on_event(event)
            triggerables.each { |c| c.on_event(event) }
        end

        def tick
            triggerables.each(&:tick)
        end

        def next_tick
            triggerables.map(&:next_tick).reject(&:nil?).min
        end

        # Returns the root of the entity tree.
        def root
            parent.nil? ? self : parent.root
        end

        # Evaluates the given script in the context of the tree root.
        def evaluate(script)
            begin
                Accessor.new(root, fuzzy: true).instance_eval(script)
            rescue Exception => e
                error(e)
            end
        end

        protected

        # Propagates an event up the tree.
        def post_event(event)
            p = parent
            unless p.nil?
                p.post_event(event)
            end
        end

        # Sends a log event with the given severity and message.
        def log(severity, message)
            post_event(LogEvent.new(self, severity, message))
        end

        # Adds a new property. Arguments are the same as for the +Property+ class.
        def self.add_property(name, **args)
            raise ArgumentError, "Name is nil" if name.nil?

            if !args.has_key?(:read) && instance_methods.include?(name)
                args[:read] = name
            end

            writer_name = (name.to_s + "=").intern
            if !args.has_key?(:write) && instance_methods.include?(writer_name)
                args[:write] = writer_name
            end

            @properties = [] if @properties.nil?
            @properties << UnboundProperty.new(name, args)
        end

        def add_property(property)
            @properties << property
        end

        # Adds a new procedure. Arguments are the same as for the +Procedure+ class.
        def self.add_procedure(name, **args)
            raise ArgumentError, "Name is nil" if name.nil?
            
            if !args.has_key?(:impl) && instance_methods.include?(name)
                args[:impl] = name
            end

            @procedures = [] if @procedures.nil?
            @procedures << UnboundProcedure.new(name, args)
        end

        def add_procedure(procedure)
            @procedures << procedure
        end

        # Call this with the name of a list of properties and a block and +property_change+ will be
        # be called if the value of any of them changes during the execution of the block.
        def modify(*prop_names)
            props = prop_names.map do |name| 
                prop = property(name)
                raise ArgumentError, "Property '#{name}' not found" if prop.nil?
                prop
            end

            values = props.map { |p| [p, p.get] }
            yield
            values.each do |property, value|
                new_value = property.get
                post_event(PropertyChangeEvent.new(property)) if value != new_value
            end
        end

        def parent=(entity)
            @parent = entity
        end

        def triggerables
            @triggerables + children.to_a
        end

        def add_triggerable(triggerable)
            @triggerables << triggerable
        end

        add_property :name, type: [ :String, nil ]
        add_property :description, type: [ :String, nil ]
        add_property :tags, type: [[ :String ]]
        add_property :class_name, type: :String, read: Proc.new { self.class.name }

        add_procedure :push_state
        add_procedure :pop_state
    end

end end