module Autodomus; module Server

    # Base event class
    class Event
        attr_reader :sender

        def initialize(sender)
            @sender = sender
        end
    end

    # Property change event.
    class PropertyChangeEvent < Event
        attr_reader :property

        def initialize(property)
            super(property.parent)
            @property = property
        end
    end

    # Log event.
    class LogEvent < Event
        attr_reader :severity
        attr_reader :message

        def initialize(sender, severity, message)
            super(sender)
            @severity = severity
            @message = message
        end
    end

end end