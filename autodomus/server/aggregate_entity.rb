module Autodomus; module Server

    # Helper class to deal with virtual entities that consist of other entities,
    # for example tags.
    class AggregateEntity < EntityBase
        def initialize(entities)
            @entities = entities
        end

        def children
            @entities.to_enum
        end

        def properties
            if @properties.nil?
                writable_props = @entities.flat_map { |e| e.properties.select(&:writable?) }
                unique_props = writable_props.uniq { |p| [p.name, p.type] }
                @properties = only_unique_names(unique_props).map do |p|
                    AggregateProperty.new(parent: self, name: p.name, type: p.type)
                end
            end

            @properties.to_enum
        end

        def procedures
            if @procedures.nil?
                all_procs = @entities.flat_map { |e| e.procedures.to_a }
                unique_procs = all_procs.uniq { |p| [p.name, p.types.to_a] }
                @procedures = only_unique_names(unique_procs).map do |p|
                    Procedure.new(parent: self, name: p.name, types: p.types)
                end
            end

            @procedures.to_enum
        end

        private

        def only_unique_names(elements)
            elements.group_by { |e| e.name }.values.select { |e| e.length == 1 }.flatten
        end

        class AggregateProperty < Property
            def set(value)
                @parent.children.each do |entity|
                    begin
                        prop = entity.property(name)
                        prop.set(value) unless prop.nil?
                    rescue AutodomusError; end
                end
            end
        end

        class AggregateProcedure < Procedure
            def invoke(*args)
                @parent.children.each do |entity|
                    begin
                        proc = entity.procedure(name)
                        proc.invoke(*args) unless proc.nil?
                    rescue AutodomusError; end
                end
            end
        end
    end

end end