require "monitor"

require "autodomus/common/exception"
require "autodomus/server/root"
require "autodomus/server/worker"
require "autodomus/server/entities"

module Autodomus; module Server

    # Maintains the execution context of Autodomus.
    class Context
        def initialize
            super()

            @monitor = Monitor.new
            @root = Root.new
            @loggers = []

            @root.on(LogEvent) { |e| log_event(e) }
        end

        # Loads entities from an array of hashes describing them.
        def load_entities(entities)
            create_child_entities(@root, entities)
        end

        # Adds a logger.
        def add_logger(logger)
            @loggers << logger
        end

        # Starts all entities.
        def start
            @root.start
        end

        # Returns the result of +describe+ for the given target.
        # Raises NotFoundError if the target was not found.
        def describe(target)
            @monitor.synchronize do
                get_target(target).describe
            end
        end

        # Sets a property for the given target which may be a path to an entity
        # or an array of tags.
        def set_property(target, key, value)
            @monitor.synchronize do
                property = get_target(target).property(key)
                raise NotFoundError, "No such property: #{key}" if property.nil?
                property.set(value)
            end
        end

         # Calls a procedure on the given target which may be the path to an entity
         # or an array of tags.
        def call_procedure(target, name, args)
            @monitor.synchronize do
                procedure = get_target(target).procedure(name)
                raise NotFoundError, "No such procedure: #{key}" if procedure.nil?
                procedure.invoke(*args)
            end
        end

        # Returns an array of all the tags.
        def tags
            @monitor.synchronize do
                @root.search.flat_map { |e| e.tags }.uniq
            end
        end

        private

        def get_target(target)
            case target
            when String then get_entity(target)
            when Array then @root.tag(*target)
            else
                raise ArgumentError, "Not a valid target: #{target.inspect}"
            end
        end

        def get_entity(path)
            entity = @root[path]
            raise NotFoundError, "No such entity" if entity.nil?
            entity
        end

        def create_entity(hash)
            args = hash.dup

            if args[:name].nil? || args[:name].empty?
                raise ConfigurationError, "No name for entity of type '#{args[:class]}"
            end

            class_name = args[:class]
            if class_name.nil?
                raise ConfigurationError, "No class specified for entity '#{args[:name]}"
            end
            args.delete(:class)

            children = args[:children]
            args.delete(:children)

            klass = begin
                Autodomus::Server.const_get(class_name)
            rescue NameError
                raise ConfigurationError, "#{class_name} is not a known class"
            end

            unless klass < Autodomus::Server::Entity
                raise ConfigurationError, "'#{klass.name}' is not an Autodomus entity"
            end

            entity = klass.new(args)
            create_child_entities(entity, children) unless children.nil?

            entity
        end

        def create_child_entities(parent, children)
            children.each do |c|
                child = create_entity(c)
                if parent.has_child?(child.name)
                    raise ConfigurationError, "Duplicate entity with name '#{child.name}'"
                end
                parent.add_child(child)
            end
        end

        def log_event(event)
            log(event.severity, "#{event.sender.path[1..-1]} - #{event.message}")
        end

        def log(severity, message)
            @loggers.each do |logger|
                logger.log(severity, message)
            end
        end
    end

end end