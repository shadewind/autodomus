require "autodomus/server/reference_scanner"

module Autodomus; module Server

    # Mixin for common behavior of items that can be triggered.
    module GenericTriggerable
        attr_accessor :interval

        def start
            unless interval.nil?
                @next_tick = Time.now + interval
            end
        end

        def next_tick
            @next_tick
        end

        # Triggers if now is after the next trigger time.
        def tick
            unless @next_tick.nil?
                while Time.now > @next_tick
                    on_trigger
                    @next_tick = @next_tick + interval
                end
            end
        end

        # Responds to the given event trigger on the given target if conditions hold.
        def on_event(event)
            on_trigger if matches(event)
        end

        # Returns true if the given event matches the triggers in this triggerable.
        def matches(event)
            return false unless event.is_a?(PropertyChangeEvent)

            triggers.any? do |trigger|
                case trigger
                when Regexp
                    trigger =~ event.property.path
                when String
                    event.property.path.end_with?(trigger)
                when Property
                    trigger == event.property
                end
            end
        end

        def add_trigger(trig)
            @triggers = [] if @triggers.nil?
            @triggers << trig unless @triggers.include?(trig)
        end

        # Searches for references to properties of an entity tree in the given script
        # and adds them as triggers.
        def triggers_from_script(script, entity)
            refs = ReferenceScanner.scan(script)
            resolve_properties(refs, entity)
        end

        # Returns an enumeration of the triggers in this triggerable.
        def triggers
            (@triggers || []).to_enum
        end

        # Configures this triggerable from the given hash for use with the given entity.
        def configure_triggerable(hash, entity)
            self.interval = timespec_to_time(hash[:interval]) if hash.has_key?(:interval)
            (hash[:triggers] || []).each { |t| add_trigger(t) }
        end

        # Resolves reference names to actual properties and adds them as triggers.
        def resolve_properties(refs, entity)
            entity.root.search.flat_map { |e| e.properties.to_a }.each do |prop|
                if refs.any? { |ref| prop.path.end_with?(ref) }
                    add_trigger(prop)
                end
            end
        end
        
        # Converts a string specifying seconds, minutes or hours to seconds as an integer.
        def timespec_to_time(timespec)
            case timespec
            when String
                if timespec.trim.downcase =~ /(\d)+\s*([smh])?/
                    return case $2
                    when "s", nil then $1.to_i
                    when "m" then $1.to_i * 60
                    when "h" then $1.to_i * 60 * 60
                    end
                end
            when Numeric
                return timespec
            end

            raise ConfigurationError, "Invalid timespec '#{timespec}'"
        end
    end

end end