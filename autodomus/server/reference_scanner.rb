require "ripper"
require "pp"

module Autodomus; module Server

    # Scans for references to properties in a Ruby script.
    module ReferenceScanner
        class << self
            # Returns an array of all the references found in the given script.
            def scan(script)
                find(Ripper.sexp(script)).uniq
            end

            private

            def find(exp)
                return [] if !exp.is_a?(Array) || exp.empty?

                case exp.first
                when :@ident
                    [ exp[1] ]
                when :vcall
                    find(exp[1])
                when :call, :field
                    [ find_string(exp[1]) + exp[2].to_s + find_string(exp[3]) ]
                else
                    exp.flat_map { |child| find(child) }
                end
            end

            def find_string(exp)
                items = find(exp)
                items.empty? ? "" : items.first.to_s
            end
        end
    end

end end