module Autodomus

    # Base class for all Autodomus exceptions.
    class AutodomusError < StandardError; end

    # Raise to indicate that the specified entity or resource does not exist.
    class NotFoundError < AutodomusError; end

    # Thrown to indicate a configuration error.
    class ConfigurationError < AutodomusError; end

end