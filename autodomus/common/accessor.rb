module Autodomus

    # Class for navigating the entity hierarchy with children as
    # properties by name. For example, if foo is the parent of bar
    # which is the parent of baz, you can do:
    #     Accessor.new(foo).bar.baz.some_property
    # In this case, some_property is delegated to +baz+.
    # TODO update comment
    class Accessor
        def initialize(target, fuzzy: false)
            @target = target
            @fuzzy = fuzzy
        end

        def parent
            Accessor.new(@target.parent)
        end

        def tag(*args)
            Accessor.new(@target.tag(*args))
        end

        def method_missing(name, *args)
            return super if @target.nil?

            if @target.respond_to?(name)
                return @target.send(name, *args)
            end

            child = get_child(name)
            return Accessor.new(child) if child

            property = @target.properties.find { |p| p.represented_methods.include?(name) }
            unless property.nil?
                if name.to_s.end_with?("=")
                    return property.set(args.first)
                else
                    return property.get
                end
            else
                procedure = @target.procedure(name)
                return procedure.invoke(*args) unless procedure.nil?
            end

            super
        end

        def respond_to_missing?(name, include_all)
            (!@target.nil? && (
                @target.respond_to?(name, include_all) ||
                get_child(name) ||
                respond_to_member?(name))) ||
            super
        end

        def respond_to_child?(name)
            @target.respond_to?(:has_child?) && @target.has_child?(name.to_s)
        end

        def respond_to_member?(name)
            @target.properties.find { |p| p.represented_methods.include?(name) } ||
            @target.procedures.find { |p| p.name == name }
        end

        def get_child(name)
            if @fuzzy
                @target.respond_to?(:[]) && @target[name]
            else
                @target.respond_to?(:child) && @target.child(name)
            end
        end
    end

end