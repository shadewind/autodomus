module Autodomus

    module LoggingHelpers
        # Sends an info log event.
        def info(message)
            log(Logger::Severity::INFO, message)
        end

        # Sends a warning log event.
        def warn(message)
            log(Logger::Severity::WARN, message)
        end

        # Sends an error log event.
        def error(message)
            log(Logger::Severity::ERROR, message)
        end
    end

end