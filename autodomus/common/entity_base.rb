module Autodomus

    # Common base functionality of entity like objects.
    class EntityBase
        # Returns the property with the specified name or +nil+ if it doesn't exist.
        def property(name)
            properties.find { |p| p.name == name.intern }
        end

        # Returns the procedure with the specified name or +nil+ if it doesn't exist.
        def procedure(name)
            procedures.find { |p| p.name == name.intern}
        end

        # Checks whether the specified property exists.
        def has_property?(name)
            return properties.any? { |p| p.name == name.intern }
        end

        # Returns a hash structure describing this entity.
        def describe
            desc = { 
                properties: properties.map(&:describe),
                procedures: procedures.map(&:describe)
            }

            desc[:children] = children.map(&:name) if respond_to?(:children)
            desc
        end
    end

end