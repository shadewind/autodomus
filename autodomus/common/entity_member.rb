require "autodomus/common/types"

module Autodomus

    class EntityMember
        attr_reader :name, :parent

        def initialize(parent = nil, name = nil)
            @parent = parent
            @name = name.intern unless name.nil?
        end

        # The path of the property or +nil+ if the parent does not have a path.
        def path
            return nil if name.nil?
            return nil if parent.nil?
            return nil unless parent.respond_to?(:path)

            parent == parent.root ? "." + name.to_s : parent.path + "." + name.to_s
        end

        def describe
            { name: name }
        end
    end

    # Represents a property.
    class Property < EntityMember
        attr_accessor :name, :type

        def initialize(parent: nil, name: nil, type: :Object)
            super(parent, name)
            @type = Types.make_type(type)
        end

        # Whether this property is readable.
        def readable?
            false
        end

        # Whether this property is writable.
        def writable?
            false
        end

        # Returns the value of the property. Default implementation returns +nil+ or throws
        # +AutodomusError+ if property is not readable.
        def get
            raise AutodomusError, "Property '#{@name}' is write-only" unless readable?
            nil
        end

        # Sets the value of the property. Default implementation does nothing but throws
        # +AutodomusError+ if the property is not writable.
        def set(value)
            raise AutodomusError, "Property '#{@name}' is read-only" unless writable?
        end

        # Returns the access type which can be +:readonly+, +:writeonly+ or +:readwrite+ or +:none+.
        def access
            if readable? && writable?
                :readwrite
            elsif readable?
                :readonly
            elsif writable?
                :writeonly
            else
                :none
            end
        end

        # Returns a JSON compatible description without writable or value.
        def describe
            desc = super.merge({
                type: type.describe,
                access: access
            })
            desc[:value] = get if readable?
            desc
        end

        # Returns an array of the methods represented by this property.
        # For a property which is both readable and writable, this will contain
        # both +name+ and +name+ concatenated with '='.
        def represented_methods
            methods = []
            methods << name if readable?
            methods << (name.to_s + "=").intern if writable?
            methods
        end
    end

    # Represents a procedure.
    class Procedure < EntityMember
        attr_accessor :name

        def initialize(parent: nil, name: nil, types: [])
            super(parent, name)
            @types = types.map { |t| Types.make_type(t) }
        end

        def types
            @types.to_enum
        end

        # The number of arguments.
        def arity
            types.length
        end

        # Returns a JSON compatible description.
        def describe
            super.merge({ types: types.map(&:describe) })
        end
    end

end