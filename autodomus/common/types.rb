require "singleton"

module Autodomus; module Types
    # Creates a new type from the given object. If the given object is
    # already a type, this simply returns the argument.
    def self.make_type(object)
        case object
        when :Object, "Object" then Types::Object.instance
        when :String, "String" then Types::String.instance
        when :Integer, "Integer" then Types::Integer.instance
        when :Float, "Float" then Types::Float.instance
        when :Boolean, "Boolean" then Types::Boolean.instance
        when :Symbol, "Symbol" then Types::Symbol.instance
        when Types::Type then object
        when ::Hash then Types::Hash.new(object)
        when ::Array
            if object.length == 1 && object.first.is_a?(::Array)
                Types::Array.new(Types::Any.new(object.first))
            else
                Types::Any.new(object)
            end
        else Types::Literal.new(object)
        end
    end

    # Thrown to indicate a conversion failed.
    class ConversionError < AutodomusError; end

    # Describes the type of a property.
    class Type
        # Converts the given object to this type or throws +ConversionError+ if this failed.
        def convert(object)
        end

        # Checks whether the given object is of this type or not.
        def is_type_of?(object)
        end

        # Returns a JSON compatible object describing this type.
        def describe
        end
    end

    class SingletonType < Type
        include Singleton
    end

    class Any < Type
        # The allowed types of this +Any+
        attr_reader :types

        def initialize(template)
            @types = template.map do |e|
                e.is_a?(Any) ? e.types : Types.make_type(e)
            end.flatten
        end

        def convert(object)
            # First try to find a perfect match
            @types.each { |t| return object if t.is_type_of?(object) }

            # Then try in the order listed
            @types.each do |t|
                begin
                    return t.convert(object)
                rescue
                end
            end
            type_string = @types.map { |t| t.describe.to_s }.join(", ")
            raise ConversionError, "#{object.to_s} cannot be converted to any of the types #{type_string}"
        end

        def is_type_of?(object)
            @types.any? { |t| t.is_type_of?(object) }
        end

        def describe
            @types.map { |t| t.describe }
        end

        def ==(other)
            other.types == self.types
        end        
    end

    class Object < SingletonType
        
        def convert(object)
            object
        end

        def is_type_of?(object)
            true
        end

        def describe
            :Object
        end
    end

    class String < SingletonType
        
        def convert(object)
            object.to_s
        end

        def is_type_of?(object)
            object.is_a?(::String)
        end

        def describe
            :String
        end
    end

    class Integer < SingletonType
        
        def convert(object)
            unless object.is_a?(::Integer) || object.is_a?(::String)
                raise ConversionError, "Only strings and integers are converted to integers"
            end

            begin
                Kernel.Integer(object)
            rescue
                raise ConversionError, "Cannot convert #{object.to_s} to an integer"
            end
        end

        def is_type_of?(object)
            object.is_a?(::Integer)
        end

        def describe
            :Integer
        end
    end

    class Float < SingletonType
        
        def convert(object)
            begin
                Kernel.Float(object)
            rescue
                raise ConversionError, "Cannot convert #{object.to_s} to a float"
            end
        end

        def is_type_of?(object)
            object.is_a?(::Float)
        end

        def describe
            :Float
        end
    end

    class Boolean < SingletonType
        
        def convert(object)
            if object.nil?
                false
            elsif [ "true", "1" ].include?(object.to_s)
                true
            elsif [ "false", "0" ].include?(object.to_s)
                false
            else
                raise ConversionError, "Cannot convert #{object.to_s} to a boolean"
            end
        end

        def is_type_of?(object)
            [TrueClass, FalseClass, NilClass].include?(object.class)
        end

        def describe
            :Boolean
        end
    end

    class Symbol < SingletonType
        
        def convert(object)
            unless object.is_a?(::String) || object.is_a?(::Symbol)
                raise ConversionError, "Only strings are converted to symbols"
            end
            object.to_s.intern
        end

        def is_type_of?(object)
            object.is_a?(::Symbol)
        end

        def describe
            :Symbol
        end
    end

    class Literal < Type
        attr_reader :value

        def initialize(value)
            # Treat strings starting with a colon as symbols as this would be lost in JSON otherwise.
            if value.is_a?(::String) && value.start_with?(":")
                @value = value[1..-1].intern
            else
                @value = value
            end
        end

        def convert(object)
            unless object.to_s == @value.to_s
                raise ConversionError, "#{object.to_s} cannot match to #{@value.to_s}" unless object.nil?
            end
            @value
        end

        def is_type_of?(object)
            @value == object
        end

        def describe
            @value.is_a?(::Symbol) ? ":#{@value.to_s}" : @value
        end

        def ==(other)
            other.value == self.value
        end
    end

    class Array < Type
        attr_reader :element_type

        def initialize(element_type)
            @element_type = Types.make_type(element_type)
        end

        def convert(object)
            unless object.is_a?(::Array)
                raise ConversionError, "Object is not an array and won't be converted"
            end
            object.map { |e| @element_type.convert(e) }
        end

        def is_type_of?(object)
            object.is_a?(::Array) && object.all? { |e| @element_type.is_type_of?(e) }
        end

        def describe
            @element_type.is_a?(Types::Any) ? [ @element_type.describe ] : [[ @element_type.describe ]]
        end

        def ==(other)
            other.element_type == self.element_type
        end
    end

    class Hash < Type
        def initialize(hash)
            @pairs = hash.map { |k,v| [Types.make_type(k), Types.make_type(v)] }
        end

        def convert(object)
            unless object.is_a?(::Hash)
                raise ConversionError, "Object is not a Hash and won't be converted"
            end

            ::Hash[object.map { |k,v| convert_pair(k, v) }.select { |p| !p.nil? }]
        end

        def is_type_of?(object)
            return false unless object.is_a?(::Hash)

            object.each_pair.all? do |k, v|
                @pairs.any? { |kt, vt| kt.is_type_of?(k) && vt.is_type_of?(v) }
            end
        end

        def describe
            ::Hash[@pairs.map { |k,v| [k.describe, v.describe] }]
        end

        def ==(other)
            other.pairs == self.pairs
        end

        protected

        attr_reader :pairs

        private

        def convert_pair(key, value)
            return [key, value] if @pairs.any? { |kt, vt| kt.is_type_of?(key) && vt.is_type_of?(value) }

            @pairs.each do |key_type, value_type|
                begin
                    return [key_type.convert(key), value_type.convert(value)]
                rescue
                end
            end

            nil
        end
    end

end end