require "socket"
require "json"

module Autodomus; module JsonRpc

    # Base class for JsonRpc errors.
    class JsonRpcError < StandardError; end

    # Raised to indicate a timeout.
    class TimeoutError < JsonRpcError; end

    # JSON RPC client over TCP
    class TcpClient
        attr_reader :host, :port
        
        def initialize(host, port)
            @host = host
            @port = port
            @mutex = Mutex.new
            @on_response = {}
            @msg_id = 0
        end

        # Gives a block to be called on notifications with message and params as
        # arguments.
        def on_notification(&block)
            @mutex.synchronize do
                @on_notification = block
            end
        end

        # Gives a block to be called when the socket is connected.
        def on_connect(&block)
            @mutex.synchronize do
                @on_connect = block
            end
        end

        # Gives a block to be called when the socket is disconnected.
        def on_disconnect(&block)
            @mutex.synchronize do
                @on_disconnect = block
            end
        end

        # Calls the given method on the server. If a block is given, calls the block with the response.
        # Otherwise, this method waits for the response.
        def call_method(*args, &block)
            connect
            method, params, timeout = args
            @mutex.synchronize do
                my_id = @msg_id
                @msg_id += 1

                response = nil
                if block.nil?
                    this_thread = Thread.current
                    @on_response[my_id] = Proc.new do |r|
                        response = r
                        this_thread.wakeup
                    end
                else
                    @on_response[my_id] = block
                end

                msg = {
                    jsonrpc: "2.0",
                    method: method,
                    id: my_id
                }
                msg[:params] = params unless params.nil?
                @socket.write(JSON.generate(msg))

                if block.nil?
                    sleep_time = 0
                    while response.nil?
                        sleep_time += @mutex.sleep
                        if !timeout.nil? && (sleep_time >= timeout)
                            @on_response.delete(my_id)
                            raise TimeoutError, "Request timed out"
                        end
                    end
                end

                response
            end
        end

        alias :[] :call_method

        # Connects the client if not already connected.
        def connect
            @mutex.synchronize do
                return unless @socket.nil? || @socket.closed?
                @reader_thread.join unless @reader_thread.nil?
                @socket = Socket.tcp(@host, @port)
                @reader_thread = Thread.new(&method(:read_loop))
            end
        end

        # Sets up auto-reconnect with the specified interval or disables it if set to +nil+.
        def auto_connect(interval)
            @mutex.synchronize do
                @auto_connect_interval = interval
                if interval.nil?
                    unless @auto_connect_thread.nil?
                        @auto_connect_thread.wakeup
                        @auto_connect_thread.join
                    end
                else
                    if @auto_connect_thread.nil?
                        @auto_connect_thread = Thread.new(&method(:auto_connect_loop))
                    end
                end
            end
        end

        # Whether the client is connected or not.
        def connected?
            @mutex.synchronize do
                !@socket.nil? && !@socket.closed?
            end
        end

        private

        def read_loop
            on_connect = @mutex.synchronize { @on_connect }
            on_connect.call unless on_connect.nil?
            begin
                @buffer = ""
                loop do
                    @buffer += @socket.readpartial(1024)
                    message, @buffer = take_message(@buffer)
                    process_message(message) unless message.nil?
                end
            rescue StandardError => e
                puts e.message
                e.backtrace.each { |e| puts "  " + e }
                @socket.close
            ensure
                on_disconnect = @mutex.synchronize { @on_disconnect }
                on_disconnect.call unless on_disconnect.nil?
            end
        end

        def auto_connect_loop
            loop do
                begin
                    connect
                rescue Errno::ECONNREFUSED
                    # Swallow
                rescue Exception => e
                    puts e.message
                    e.backtrace.each { |e| puts "  " + e }
                end

                sleep_time = @mutex.synchronize { @auto_connect_interval }
                return if sleep_time.nil?
                sleep sleep_time
            end
        end

        def take_message(data)
            depth = 0
            data.each_char.each_with_index do |c, i|
                if (depth == 0) && (c != "{")
                    raise JsonRpcError, "Data does not start with '{'"
                end

                case c
                when "{" then depth += 1
                when "}" then depth -= 1
                end

                return data[0..i], data[(i + 1)..-1] if depth == 0
            end

            [ nil, @buffer ]
        end

        def process_message(data)
            message = JSON.parse(data)
            
            unless message.is_a?(Hash)
                raise JsonRpcError, "Message must be a hash"
            end

            if message.has_key?("id")
                process_response(message)
            else
                process_notification(message)
            end
        end

        def process_response(message)
            id = message["id"]
            response_callback = @mutex.synchronize { @on_response.delete(id) }
            response_callback.call(message["result"]) unless response_callback.nil?
        end

        def process_notification(message)
            unless message.has_key?("method")
                raise JsonRpcError, "Message has no method key"
            end

            unless message.has_key?("params")
                raise JsonRpcError, "Message has no param key"
            end

            @mutex.synchronize do
                unless @on_notification.nil?
                    @on_notification.call(message["method"], message["params"])
                end
            end
        end
    end

end end