module Autodomus

    module EntityNavigation
        # Calls the given block for each entity recursively. If no block is given,
        # returns an enum.
        def search(&block)
            return to_enum(:search) if block.nil?
            yield(self)
            children.each { |c| c.search(&block) }
        end

        # Resolves the given path to a node in the tree. A path has the form:
        #     nodeA.nodeB.nodeC
        # The path above will match any nodeC which is a child of nodeB which is a child of
        # node nodeA. If the path is preceded by a dot, the path is assumed to start at this node.
        #
        # An empty path resolves to this node.
        def [](p)
            p = p.to_s
            return self if p.empty? || p == "."

            components = p.split(".").select { |c| !c.empty? }
            child = child(components.first)
            if child.nil?
                if p.start_with?(".")
                    nil
                else
                    children.each do |c|
                        result = c[p]
                        return result unless result.nil?
                    end
                    nil
                end
            else
                child["." + components[1..-1].join(".")]
            end
        end

        # Returns the path of this entity.
        def path
            "." + array_path.join(".")
        end

        # Returns the path of this entity as an array where each element is
        # the name of an entity.
        def array_path
            if parent.nil?
                []
            else
                parent.array_path + [ name ]
            end
        end

        # Returns the child entity with the specified name.
        def child(name)
            children.find { |e| e.name == name }
        end

        # Returns true if this entity has a child with the specified name.
        def has_child?(name)
            children.any? { |e| e.name == name }
        end
    end

end