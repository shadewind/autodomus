$LOAD_PATH.unshift(File.dirname(__FILE__))

require "net/http"
require "json"
require "forwardable"

require "autodomus/common/exception"
require "autodomus/common/entity_nav"
require "autodomus/common/entity_member"
require "autodomus/common/entity_base"

module Autodomus

    class PropertyProxy < Property
        def initialize(access: :none, **args)
            super(args)
            @access = access
        end

        def readable?
            [ :readonly, :readwrite ].include?(@access)
        end

        def writable?
            [ :writeonly, :readwrite ].include?(@access)
        end

        def get
            unless readable?
                raise AutodomusError, "Property '#{name}' is write-only"
            end

            info = parent.client.get(parent.resource_path)
            property_array = info["properties"]

            if property_array.is_a?(Array)
                hash = property_array.find { |p| p.is_a?(Hash) && p["name"] == name.to_s }
                return nil if hash.nil?
                begin
                    return type.convert(property_hash["value"])
                rescue ConversionError => e
                    raise ClientError, "Server value does not match type: #{e.message}"
                end
            end

            nil
        end

        def set(value)
            parent.client.action(parent.resource_path, { set_properties: { name => value } })
            nil
        end
    end

    class ProcedureProxy < Procedure
        def invoke(*args)
            parent.client.action(parent.resource_path, { call_procedures: { name => args } })
            nil
        end
    end

    # Base proxy class for server resources.
    class ResourceProxy < EntityBase
        # The associated client
        attr_reader :client

        def initialize(client)
            @client = client
        end

        def properties
            get_self if @properties.nil?
            @properties.to_enum
        end

        def procedures
            get_self if @procedures.nil?
            @procedures.to_enum
        end

        protected

        # Retrieves data from server and populates data variables. Returns the data.
        def get_self
            info = @client.get(resource_path)
            parse_properties(info["properties"])
            parse_procedures(info["procedures"])
            info
        end

        def parse_properties(property_info)
            @properties = []

            return unless property_info.is_a?(Array)
            property_info.each do |p|
                if p.is_a?(Hash)
                    @properties << PropertyProxy.new(
                        parent: self,
                        name: p["name"].to_s,
                        access: (p["access"] || :none).intern,
                        type: p["type"] || :Object
                    )
                end
            end
        end

        def parse_procedures(procedure_info)
            @procedures = []

            return unless procedure_info.is_a?(Array)
            procedure_info.each do |p|
                if p.is_a?(Hash)
                    types = p["types"] || []
                    next unless types.is_a?(Array)
                    @procedures << ProcedureProxy.new(
                        parent: self,
                        name: p["name"].to_s, 
                        types: types
                    )
                end
            end
        end
    end

    # Proxy class which represents an entity on the server.
    class EntityProxy < ResourceProxy
        include EntityNavigation

        # The name of the entity
        attr_reader :name
        # The parent entity
        attr_reader :parent

        def initialize(client, name, parent)
            super(client)
            @name = name
            @parent = parent
        end

        # The children entities (proxies).
        def children
            get_self if @children.nil?
            @children.to_enum
        end

        # Returns the child entity proxy with the given name.
        def child(name)
            children.find { |c| c.name == name }
        end

        # The path of the entity on the server.
        def resource_path
            "/entity" + path.gsub(".", "/")
        end

        protected

        def get_self
            info = super

            child_info = info["children"]
            if child_info.is_a?(Array)
                @children = child_info.map { |c| EntityProxy.new(@client, c.to_s, self) }
            else
                @children = []
            end

            info
        end
    end

    # Proxy which represents a tag combination on the server.
    class TagProxy < ResourceProxy

        def initialize(client, tags)
            super(client)
            @tags = tags
        end

        # The path of the tag on the server.
        def resource_path
            "/tags?" + @tags.join('&')
        end
    end

    # Autodomus client class.
    class Client < EntityProxy

        # Creates a new Autodomus client using the specified base URL.
        def initialize(url)
            super(self, nil, nil)

            url = URI(url) unless url.is_a?(URI)
            @base = url.path.chomp("/")
            @http = Net::HTTP.new(url.hostname, url.port)
        end

        # Returns a proxy for the given tags.
        def tag(*tags)
            TagProxy.new(self, tags)
        end

        def get(path)
            handle_response(@http.request_get(@base + path))
        end

        def action(path, *actions)
            response = @http.request_post(
                @base + path,
                JSON.generate(actions),
                { "content-type" => "application/json" })
            handle_response(response)
        end

        def handle_response(response)
            case response
            when Net::HTTPSuccess
                begin
                    JSON.parse(response.body)
                rescue JSON::JSONError => e
                    raise AutodomusError, "JSON parse error: #{e.message}"
                end
            when Net::HTTPNotFound
                raise NotFoundError, "The resource does not exist on the server: #{response.body}"
            else
                raise AutodomusError, "Unexpected #{response.code} response from server: #{response.body}"
            end
        end
    end

end